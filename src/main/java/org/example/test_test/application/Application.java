package org.example.test_test.application;


import org.example.test_test.database.WorkWithDB;
import org.example.test_test.entity.User;
import org.example.test_test.filejob.FileWorkerJsonImpl;
import org.example.test_test.reportservice.ReportService;
import org.example.test_test.watcher.FileWatcher;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Logger;


public class Application {
    private static final Logger logger = Logger.getGlobal();
    public static void main(String[] args) throws IOException, InterruptedException, SQLException {
        boolean param = false;
        for (String s : args) {
            if (s.equalsIgnoreCase("display")) {
                param = true;
            }
        }
        if (param) {
            WorkWithDB workWithDB=new WorkWithDB();
            System.out.println("Количество записей в БД: "+workWithDB.selectAllRawsFromDB());
            ReportService reportService=new ReportService();
            reportService.WriteFile();
        } else {
            for (; ; ) {
                FileWatcher fileWatcher = new FileWatcher();
                boolean end = fileWatcher.fileWatch().endsWith("json");
                if (end) {
                    System.out.println("This is JSON file");
                    FileWorkerJsonImpl fileWorkerJson = new FileWorkerJsonImpl();
                    User[] users = fileWorkerJson.getUsersFromFile(fileWatcher.fileWatch());
                    System.out.println(users.length);
                    WorkWithDB workWithDB = new WorkWithDB();
                    System.out.println(workWithDB.InsertDB(users));
                } else {
                    logger.info(fileWatcher.fileWatch());
                }
            }
        }
    }
}
