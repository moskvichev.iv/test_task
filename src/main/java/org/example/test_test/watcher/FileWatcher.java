package org.example.test_test.watcher;

import java.io.IOException;
import java.nio.file.*;
import java.util.List;
import java.util.logging.Logger;

public class FileWatcher {

    public String fileWatch() throws IOException, InterruptedException {
        Path dirPath = Paths.get(".\\src\\main\\java\\org\\example\\test_test\\test_temp");
        //Если такого каталога не существует, то создаем его + создается обработка исключительной ситуации
        if (!Files.exists(dirPath)) Files.createDirectories(dirPath);
        //Сервис, который следит за изменениями в каталоге
        WatchService watchService = FileSystems.getDefault().newWatchService();
        //Регистрация сервиса на каталоге
        dirPath.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);
        WatchKey key;
        String filePathToStr="";
        //while ((key = watchService.take()) != null) {
        if ((key = watchService.take()) != null) {
            for (WatchEvent<?> event : key.pollEvents()) {
                if (event.kind() == StandardWatchEventKinds.ENTRY_CREATE) {
                    Path filePath = ((WatchEvent<Path>) event).context();
                    System.out.println(filePath);
                    filePathToStr = filePath.toString();
                } else {
                    System.out.println("Nothing");
                }
            }
            key.reset();
        }
        //}
        return filePathToStr;
    }
}
