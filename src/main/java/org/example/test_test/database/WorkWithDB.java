package org.example.test_test.database;

import org.example.test_test.entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WorkWithDB {
    static final String DB_URL = "jdbc:postgresql://localhost:5432/test_task";
    static final String USER = "postgres";
    static final String PASS = "sasasasa";
    public int InsertDB(User[] users) throws SQLException {
        //Удаление дубликатов по имени путём создания Map с использованием имени в качестве ключа
        Map<String, User> names = new HashMap<String, User>();
        int avgsal = 0;
        for (User u: users
        ) {
            names.put(u.getName(),u);
            avgsal+=u.getSalary();
        }
        for(Map.Entry<String, User> item : names.entrySet()){
            if (item.getValue().getSalary()>avgsal/users.length) {
            insertIntoDB(item.getValue().getName(),item.getValue().getAge(),item.getValue().getSalary());
            }
        }

        return  avgsal/users.length;
    }
    private static Connection getDBConnection() {
        Connection dbConnection = null;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        try {
            dbConnection = DriverManager.getConnection(DB_URL, USER, PASS);
            return dbConnection;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return dbConnection;
    }
    public void insertIntoDB(String nm,int ag,int sal) throws SQLException {
        Connection dbConnection = null;
        PreparedStatement statement = null;

        String insertTableSQL = "INSERT INTO test_task_user (name,age,salary) VALUES (?,?,?)";

        try {
            dbConnection = getDBConnection();
            statement = dbConnection.prepareStatement(insertTableSQL);
            statement.setString(1,nm);
            statement.setInt(2,ag);
            statement.setInt(3,sal);
            // выполнить SQL запрос
            statement.execute();
            System.out.println("Записи добавлены");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
    }
    public ArrayList<User> selectFromDB(){
        Connection dbConnection = null;
        Statement statement = null;
        String selectTableSQL = "SELECT * FROM test_task_user WHERE age>25 ORDER BY age";
        ArrayList<User> users=new ArrayList<>();
        try {
            dbConnection = getDBConnection();
            statement = dbConnection.createStatement();

            // выбираем данные с БД
            ResultSet rs = statement.executeQuery(selectTableSQL);

            // И если что то было получено то цикл while сработает

            while (rs.next()) {
                String username = rs.getString("name");
                int userage = rs.getInt("age");
                int usersalary=rs.getInt("salary");
                users.add(new User(username,userage,usersalary));
            }
            System.out.println("Запрос выполнен");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return users;
    }
    public int selectAllRawsFromDB(){
        Connection dbConnection = null;
        Statement statement = null;
        String selectTableSQL = "SELECT COUNT (*) FROM test_task_user";
        int count=0;
        try {
            dbConnection = getDBConnection();
            statement = dbConnection.createStatement();
            // выбираем данные с БД
            ResultSet rs = statement.executeQuery(selectTableSQL);
            if (rs.next()) {
                count = rs.getInt("count");
            } else {
                System.out.println("Записей нет");
            }
            System.out.println(count);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return count;
    }

}
