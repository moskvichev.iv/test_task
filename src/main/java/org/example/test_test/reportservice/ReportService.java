package org.example.test_test.reportservice;

import org.example.test_test.database.WorkWithDB;
import org.example.test_test.entity.User;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class ReportService {
    public void WriteFile() throws IOException {
        WorkWithDB workWithDB=new WorkWithDB();
        Path dirPath = Paths.get(".\\src\\main\\java\\org\\example\\test_test\\report");
        //Если такого каталога не существует, то создаем его + создается обработка исключительной ситуации
        if (!Files.exists(dirPath)) Files.createDirectories(dirPath);
        ArrayList<User> users = workWithDB.selectFromDB();

        try(FileWriter writer = new FileWriter(".\\src\\main\\java\\org\\example\\test_test\\report\\report.txt", false))
        {
            // запись всей строки
            String text="";
            for (User u:workWithDB.selectFromDB()
                 ) {
                text=u.getName()+" "+u.getAge()+" "+u.getSalary()+"\n";
                writer.write(text);
            }

            writer.flush();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }
    }

}
