package org.example.test_test.filejob;

import org.example.test_test.entity.User;

import java.util.HashMap;


public interface FileWorker {
    public User[] getUsersFromFile(String path);
}
