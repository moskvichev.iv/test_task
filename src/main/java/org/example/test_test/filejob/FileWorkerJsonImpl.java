package org.example.test_test.filejob;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import lombok.SneakyThrows;
import org.example.test_test.entity.User;
import org.example.test_test.watcher.FileWatcher;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class FileWorkerJsonImpl implements FileWorker {
    @SneakyThrows
    public User[] getUsersFromFile(String path) {
        JsonReader reader = new JsonReader(new FileReader(".\\src\\main\\java\\org\\example\\test_test\\test_temp\\"+path));
        User[] users = new Gson().fromJson(reader, User[].class);
        return users;
    }
}
