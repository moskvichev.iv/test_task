CREATE DATABASE test_task
CREATE TABLE test_task_user (
	id SERIAL PRIMARY KEY,
	name CHARACTER VARYING (20),
	age INT,
	salary INT
);